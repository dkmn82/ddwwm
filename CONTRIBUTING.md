Entwickler:
 - Dennis Richter <dennis.richter@iem.thm.de>
   - Einlesen und verarbeiten des Fragenkataloges
   - Spiellogik

 - Denis Kamenev <denis.kamenev@iem.thm.de>
   - Aufbereiten und anzeigen der Fragen
   - Spiellogik
