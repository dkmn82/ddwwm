# Wer wird Millonär?
Dieses Programm stellt die allseits bekannte TV-Show "Wer wird Millonär?" nach.

## Der Ablauf
Sie können eine Million Euro gewinnen (natürlich nicht wirklich), wenn sie zu 15 Fragen mit vier vorgegebenen Antwortmöglichkeiten immer die richtige Antwort wählen. Jede dieser Fragen ist mit einer Gewinnsumme verbunden. Dabei stehen dem Spieler verschiedene Joker zur Verfügung. Das Spiel ist beendet, sobald der Kandidat eine Frage falsch beantwortet, die Millionenfrage löst oder aber freiwillig aussteigt. Bei Ersterem erhält der Spieler die Gewinnsumme der letzten durchschrittenen Sicherheitsstufe, bei Zweiterem eine Millionen Euro und bei letzterem die zur letzten beantworteten Frage gehörige Gewinnsumme.

### Gewinntabelle
| Frage |    Gewinn   | Sonstiges                                                       |
| ----: | ----------: | :-------------------------------------------------------------- |
|    1  |        50 € |                                                                 |
|    2  |       100 € |                                                                 |
|    3  |       200 € |                                                                 |
|    4  |       300 € |                                                                 |
|    5  |       500 € | 1. Sicherheitsstufe                                             |
|    6  |     1.000 € |                                                                 |
|    7  |     2.000 € |                                                                 |
|    8  |     4.000 € |                                                                 |
|    9  |     8.000 € |                                                                 |
|   10  |    16.000 € | 2. Sicherheitsstufe (Bei verzicht erhält Spieler 3. Joker)      |
|   11  |    32.000 € |                                                                 |
|   12  |    64.000 € |                                                                 |
|   13  |   125.000 € |                                                                 |
|   14  |   500.000 € |                                                                 |
|   15  | 1.000.000 € |                                                                 |

### Joker
Dem Kandidaten stehen zwei Joker zur Verfügung, die ihm bei der Beantwortung der Fragen helfen können. Wählt er vorab die Risikovariante, wird ein dritter – sogenannter Zusatzjoker – geschaltet. Dabei darf jeder Joker nur einmal eingesetzt werden. Der Kandidat hat auch die Möglichkeit, die Joker miteinander zu verknüpfen. So kann er durchaus zum Beispiel zunächst den 50:50-Joker und direkt im Anschluss den Telefonjoker benutzen.

#### 50:50-Joker
Dieser Joker blendet zwei falsche Antworten nach dem Zufallsprinzip aus.

#### Telefonjoker
In dieser Version des Spiels hat der Spieler die Möglichkeit eine Lösung gesagt zu bekommen, die mit einer Wahrscheinlichkeit von 70% richtig ist.

#### Zusatzjoker (bei verzicht der 2. Sicherheitsstufe)
In dieser Version des Spiels hat der Spieler die Möglichkeit eine Minute lang das Internet benutzen zu dürfen.

## Herausforderungen bei der Entwicklung
Die vermutlich größte technische Herausforderung wird das Einlesen und Verarbeiten des Fragenkataloges.
