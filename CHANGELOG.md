## 1.0.0 (2016-01-09) | stable

* Add the 1 mill win interface
* Fix some bugs
* Add new questions

## 0.1.7 (2016-01-09)

* Change console settings
* Create joker functions/interfaces
* Fix many minor bugs
* Last interface changes
* Play the game

## 0.1.6 (2016-01-07)

* Declare all functions in use
* Implement audio
* Database.txt fix
* Questions and shuffle answers
* Create game logic
* Update interfaces

## 0.1.5 (2016-01-06)

* Implement answers in interface
* Implement audio

## 0.1.4 (2016-01-05)

* ASCII fix works
* Set global variables
* Create main interface
* Database ASCII fix

## 0.1.3 (2016-01-03)

* Create database for questions
* Create first interface
* Create first concept of database
* Minor interface fixes
* Try to apply ASCII fix on interface

## 0.1.2 (2016-01-02)

* Implement function to show right ASCII signs
* Create .gitignore

## 0.1.1 (2015-12-17)

* Create VisualStudio project
* Update README.md
* Implement console colors
* Implement first interface


## 0.1.0 (2015-12-09)

* Create README.md
* Create CONTRIBUTING.md
* Create CHANGELOG.md
