﻿// WerWirdMillonaer.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include <windows.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <MMsystem.h>

#define GREEN 0x0002
#define RED   0x0004
#define BLUE  0x0003
#define ORANGE 0x0006

#define EASY   0
#define MIDDLE 1
#define HARD   2

/*********************************
 * Console hacks                 *
 *********************************/

 // Umlauts
#define AE (unsigned char)142
#define ae (unsigned char)132
#define OE (unsigned char)153
#define oe (unsigned char)148
#define UE (unsigned char)154
#define ue (unsigned char)129
#define ss (unsigned char)225

#define CTL  (unsigned char)218
#define CTR  (unsigned char)191
#define CBL  (unsigned char)192
#define CBR  (unsigned char)217
#define HL   (unsigned char)196
#define VL   (unsigned char)179
#define TL   (unsigned char)195
#define TR   (unsigned char)180
#define BULL (unsigned char)254

// Console handels
HANDLE console;
WORD   consoleSettings;

// Global variables
char ***data_easy = (char***)calloc(10, sizeof(char**));
int i_easy = 0;
char ***data_middle = (char***)calloc(10, sizeof(char**));
int i_middle = 0;
char ***data_hard = (char***)calloc(10, sizeof(char**));
int i_hard = 0;

int risiko = 0;
int fiftyfifty = 1;
int telefon = 1;
int internet = 0;
int score = 1;
int currentRightAnswer = -1;


// Make all function knowing
void prepareConsole();
void setColor(int color);
void resetColor();
char* fixCoding(char *string);
char** str_split(char* str, const char a_delim, int count);
int random(int max);

int showRiskInterface();
void showQuestion(char *question, char *answers[4], bool bubble = false);
int showGameInterface(char *question, char *answers[4], bool bubble = false);
int showJokerInterface(char *question, char *answers[4]);
void showEndInterface(int score, bool happy);

void createDatabase(char *path);
void loadDatabase(char *path);
char** getRandomQuestion(int level, int ctr = 0);

/**
 * Prepares the console for colors
 */
void prepareConsole() {

  console = GetStdHandle(STD_OUTPUT_HANDLE);

  // Set console title
  char text[] = "Wer Wird Millionär? - Dennis Richter & Denis Kamenev";
  wchar_t wtext[55];
  mbstowcs_s(0, wtext, 55, text, _TRUNCATE);
  SetConsoleTitle(wtext);

  // Set console size
  SMALL_RECT windowSize = { 0, 0, 116, 35 };
  SetConsoleWindowInfo(console, 1, &windowSize);
  COORD bufferSize = { 116, 35 };
  SetConsoleScreenBufferSize(console, bufferSize);

  // Save current settings
  CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
  GetConsoleScreenBufferInfo(console, &consoleInfo);
  consoleSettings = consoleInfo.wAttributes;
}

/**
 * Set a console color
 */
void setColor(int color) {
  SetConsoleTextAttribute(console, color);
}

/**
 * Resets the console colors
 */
void resetColor() {
  SetConsoleTextAttribute(console, consoleSettings);
}

/**
 * Fix the coding
 */
char* fixCoding(char *string) {
  int len = strlen(string) + 1;
  char *fixed = new char[len];
  strcpy_s(fixed, len, string);

  for(int i = 0; i < strlen(string); i++) {
    switch(string[i]) {
    case 'Ä':
      fixed[i] = AE;
      break;

    case 'ä':
      fixed[i] = ae;
      break;

    case 'Ö':
      fixed[i] = OE;
      break;

    case 'ö':
      fixed[i] = oe;
      break;

    case 'Ü':
      fixed[i] = UE;
      break;

    case 'ü':
      fixed[i] = ue;
      break;

    case 'ß':
      fixed[i] = ss;
      break;

    case '^':
      fixed[i] = CTL;
      break;

    case '´':
      fixed[i] = CTR;
      break;

    case '°':
      fixed[i] = CBL;
      break;

    case '`':
      fixed[i] = CBR;
      break;

    case '=':
      fixed[i] = HL;
      break;

    case '|':
      fixed[i] = VL;
      break;

    case '>':
      fixed[i] = TL;
      break;

    case '<':
      fixed[i] = TR;
      break;

    case '³':
      fixed[i] = BULL;
      break;

    }
  }

  return fixed;
}

char** str_split(char* str, const char a_delim, int count) {
  char **result;
  char  *token;
  char  *rest = str;
  char  delim[] = { a_delim, 0 };

  result = (char**)malloc(sizeof(char*) * count);


  for(int i = 0; (token = strtok_s(rest, delim, &rest)) || i < count; i++) {
    *(result + i) = _strdup(token);
  }

  return result;
}

int random(int max) {
  return rand() % max;
}


/*********************************
 * Game control functions        *
 *********************************/

 /**
  * Run the game, contains the game logic
  */
void startGame() {
  risiko = showRiskInterface();

  for(int i = 1; i <= 15; i++) {
    score = i;

    int level = EASY;
    if(i > 10)
      level = HARD;
    else if(i > 5)
      level = MIDDLE;

    // Getting a random question
    char **view_data = getRandomQuestion(level);
    if(view_data == NULL) return;

    int right = random(4);
    char *answers[4];
    answers[0] = view_data[right + 2];
    answers[1] = view_data[right == 1 ? 2 : 3];
    answers[2] = view_data[right == 2 ? 2 : 4];
    answers[3] = view_data[right == 3 ? 2 : 5];

    currentRightAnswer = right;
    int answer = showGameInterface(view_data[1], answers);

    // User wants to exit
    if(answer == -1)
      return showEndInterface(score - 1, true);

    // The users answer is wrong
    if(answer != right)
      return showEndInterface(score - 1, false);
  }

  // The user is a big pro, he gets the million
  showEndInterface(score, true);
}

/*********************************
 * Interface control functions   *
 *********************************/

 /**
  * The wwm logo for loading view
  */
void showLoadingInterface() {
  system("cls");
  printf("\n\n\n\n\n\n");
  printf(fixCoding("                ________                         __          __  \n"));
  printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  | \n"));
  printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  | \n"));
  printf(fixCoding("               |________||_____|__|   |________||__||__| |_____| \n"));
  printf("\n");
  printf(fixCoding("               _______ __ __ __              .==.==.       _____ \n"));
  printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |\n"));
  printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='\n"));
  printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|  \n"));
  printf(fixCoding("                                                           '=='  \n"));
  printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
}

/**
 * The start interface asks the risk variant
 */
int showRiskInterface() {
  char *output[34];
  output[0]  = "     ^=========´^=========´^=====/====´                                            ^==============================´\n";
  output[1]  = "     |  50:50  || Telefon || Int/rnet |                                            |                              |\n";
  output[2]  = "     °=========`°=========`°===/======`                                            >==    15 ³ 1.000.000 EUR    ==<\n";
  output[3]  = "                                                                                   |                              |\n";
  output[4]  = "                                                                                   >==    14 ³   500.000 EUR    ==<\n";
  output[5]  = "                                                                                   |                              |\n";
  output[6]  = "                ________                         __          __                    >==    13 ³   125.000 EUR    ==<\n";
  output[7]  = "               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |                              |\n";
  output[8]  = "               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==    12 ³    64.000 EUR    ==<\n";
  output[9]  = "               |________||_____|__|   |________||__||__| |_____|                   |                              |\n";
  output[10] = "                                                                                   >==    11 ³    32.000 EUR    ==<\n";
  output[11] = "               _______ __ __ __              .==.==.       _____                   |                              |\n";
  output[12] = "              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==    10 ³    16.000 EUR    ==<\n";
  output[13] = "              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |                              |\n";
  output[14] = "              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==     9 ³     8.000 EUR    ==<\n";
  output[15] = "                                                           '=='                    |                              |\n";
  output[16] = "                                                                                   >==     8 ³     4.000 EUR    ==<\n";
  output[17] = "                                                                                   |                              |\n";
  output[18] = "     ^===================================================================´         >==     7 ³     2.000 EUR    ==<\n";
  output[19] = "     |              Möchten Sie die Risiko Variante spielen?             |         |                              |\n";
  output[20] = " ====<  Achtung: Bei Risiko entfällt die Sicherheitsstufe bei Frage 10!  >====     >==     6 ³     1.000 EUR    ==<\n";
  output[21] = "     |         Die Sicherheitsstufe bei Frage 5 bleibt erhalten!         |         |                              |\n";
  output[22] = "     °===================================================================`         >==     5 ³       500 EUR    ==<\n";
  output[23] = "     ^=============================================´         ^===========´         |                              |\n";
  output[24] = " ====< ³ J: Ja, ich will den Internet-Zusatzjoker! >=========< ³ N: Nein >====     >==     4 ³       300 EUR    ==<\n";
  output[25] = "     °=============================================`         °===========`         |                              |\n";
  output[26] = "                                                                                   >==     3 ³       200 EUR    ==<\n";
  output[27] = "                                                                                   |                              |\n";
  output[28] = "                                                                                   >==     2 ³       100 EUR    ==<\n";
  output[29] = "                                                                                   |                              |\n";
  output[30] = "                         ^===´^===´                                                >==     1 ³        50 EUR    ==<\n";
  output[31] = " Mögliche Eingaben sind: | j || n |                                                |                              |\n";
  output[32] = "                         °===`°===`                                                °==============================`\n";
  output[33] = " Ihre Eingabe: ";

  system("cls");
  for(int i = 0; i < 34; i++) {
    printf(fixCoding(output[i]));
  }

  char input = getchar();

  // To consume `\n`
  if(input != '\n') getchar();

  if(input != 'j' && input != 'n')
    return showRiskInterface();

  return input == 'j' ? internet = 1 : 0;
}

/**
 * Shows the question perfekt center aligned
 */
void showQuestion(char *question, char *answers[4], bool bubble) {
  int len = strlen(question);

  char *middle;
  char *top;
  char *bottom;

  int middle_left_space = 65;
  int middle_right_space = 0;
  int top_left_space = 65;
  int top_right_space = 0;
  int bottom_left_space = 65;
  int bottom_right_space = 0;

  if(len <= 65) {
    middle = question;
    top = "";
    bottom = "";

    middle_left_space = (65 + strlen(middle)) / 2;
    middle_right_space = 65 - middle_left_space;
  }
  else {
    int i;
    for(i = len / 2; i < len; i++) {
      if(question[i] == ' ')
        break;
    }

    middle = "";

    // Init
    top = new char[i + 1];
    bottom = new char[len - i + 1];

    // Copy (dst, dst_length, src, end);
    strncpy_s(top, i + 1, question, i);
    strncpy_s(bottom, len - i + 1, question + i, len - 1);

    // Calc left and right space
    top_left_space = (65 + strlen(top)) / 2;
    top_right_space = 65 - top_left_space;

    bottom_left_space = (65 + strlen(bottom)) / 2;
    bottom_right_space = 65 - bottom_left_space;
  }

  // Bubble answer
  // First check if it is 50:50 joker
  bool ff = false;
  for(int i = 0; i < 4; i++) {
    if(answers[i] == "") {
      ff = true;
      break;
    }
  }

  // Get right with 70% or it is a 50:50 joker
  int wrong = random(100) > 70 && !ff ? random(3) : 0;

  char a[] = "ABCDABCD";
  char correctAnswer = a[currentRightAnswer + wrong];

  system("cls");
  printf(fixCoding("     ^=====%c%s´^=====%c%s´^=====%c%s=´                                            ^==============================´\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
  printf(fixCoding("     |  50%c%s || Tel%c%s || Int%c%st |                                            |    %s    |\n"), fiftyfifty ? ':' : '/', "50 ", telefon ? 'e' : '/', "fon", internet ? 'e' : '/', "rne", score == 15 ? fixCoding("^====================´") : "                      ");
  printf(fixCoding("     °===%c%s==`°===%c%s==`°===%c%s===`                                            >==%s 15 ³ 1.000.000 EUR %s==<\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="), score == 15 ? fixCoding("==<") : "   ", score == 15 ? fixCoding(">==") : "   ");
  printf(fixCoding(" %43s                                       |    %s%s%s    |\n"), bubble ? ".-----------------------------------------." : "", score == 14 ? fixCoding("^====================´") : "", score == 15 ? fixCoding("°====================`") : "", score != 14 && score != 15 ? "                      " : "");
  printf(fixCoding("%41s%c%s                                       >==%s 14 ³   500.000 EUR %s==<\n"), bubble ? "<  Ich denke die richtige Antwort lautet " : "", bubble ? correctAnswer : ' ', bubble ? " |" : "  ", score == 14 ? fixCoding("==<") : "   ", score == 14 ? fixCoding(">==") : "   ");
  printf(fixCoding(" %43s                                       |    %s%s%s    |\n"), bubble ? "`-----------------------------------------'" : "", score == 13 ? fixCoding("^====================´") : "", score == 14 ? fixCoding("°====================`") : "", score != 13 && score != 14 ? "                      " : "");
  printf(fixCoding("                ________                         __          __                    >==%s 13 ³   125.000 EUR %s==<\n"), score == 13 ? fixCoding("==<") : "   ", score == 13 ? fixCoding(">==") : "   ");
  printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |    %s%s%s    |\n"), score == 12 ? fixCoding("^====================´") : "", score == 13 ? fixCoding("°====================`") : "", score != 12 && score != 13 ? "                      " : "");
  printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==%s 12 ³    64.000 EUR %s==<\n"), score == 12 ? fixCoding("==<") : "   ", score == 12 ? fixCoding(">==") : "   ");
  printf(fixCoding("               |________||_____|__|   |________||__||__| |_____|                   |    %s%s%s    |\n"), score == 11 ? fixCoding("^====================´") : "", score == 12 ? fixCoding("°====================`") : "", score != 11 && score != 12 ? "                      " : "");
  printf(fixCoding("                                                                                   >==%s 11 ³    32.000 EUR %s==<\n"), score == 11 ? fixCoding("==<") : "   ", score == 11 ? fixCoding(">==") : "   ");
  printf(fixCoding("               _______ __ __ __              .==.==.       _____                   |    %s%s%s    |\n"), score == 10 ? fixCoding("^====================´") : "", score == 11 ? fixCoding("°====================`") : "", score != 10 && score != 11 ? "                      " : "");
  printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==%s 10 "), score == 10 ? fixCoding("==<") : "   "); if(!risiko) setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("    16.000 EUR %s==<\n"), score == 10 ? fixCoding(">==") : "   ");
  printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |    %s%s%s    |\n"), score == 9 ? fixCoding("^====================´") : "", score == 10 ? fixCoding("°====================`") : "", score != 9 && score != 10 ? "                      " : "");
  printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==%s  9 ³     8.000 EUR %s==<\n"), score == 9 ? fixCoding("==<") : "   ", score == 9 ? fixCoding(">==") : "   ");
  printf(fixCoding("                                                           '=='                    |    %s%s%s    |\n"), score == 8 ? fixCoding("^====================´") : "", score == 9 ? fixCoding("°====================`") : "", score != 8 && score != 9 ? "                      " : "");
  printf(fixCoding("                                                                                   >==%s  8 ³     4.000 EUR %s==<\n"), score == 8 ? fixCoding("==<") : "   ", score == 8 ? fixCoding(">==") : "   ");
  printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 7 ? fixCoding("^====================´") : "", score == 8 ? fixCoding("°====================`") : "", score != 7 && score != 8 ? "                      " : "");
  printf(fixCoding("     ^===================================================================´         >==%s  7 ³     2.000 EUR %s==<\n"), score == 7 ? fixCoding("==<") : "   ", score == 7 ? fixCoding(">==") : "   ");
  printf(fixCoding("     | %*s%*s |         |    %s%s%s    |\n"), top_left_space, fixCoding(top), top_right_space, "", score == 6 ? fixCoding("^====================´") : "", score == 7 ? fixCoding("°====================`") : "", score != 6 && score != 7 ? "                      " : "");
  printf(fixCoding(" ====< %*s%*s >====     >==%s  6 ³     1.000 EUR %s==<\n"), middle_left_space, fixCoding(middle), middle_right_space, "", score == 6 ? fixCoding("==<") : "   ", score == 6 ? fixCoding(">==") : "   ");
  printf(fixCoding("     | %*s%*s |         |    %s%s%s    |\n"), bottom_left_space, fixCoding(bottom), bottom_right_space, "", score == 5 ? fixCoding("^====================´") : "", score == 6 ? fixCoding("°====================`") : "", score != 5 && score != 6 ? "                      " : "");
  printf(fixCoding("     °===================================================================`         >==%s  5 "), score == 5 ? fixCoding("==<") : "   "); setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("       500 EUR %s==<\n"), score == 5 ? fixCoding(">==") : "   ");
  printf(fixCoding("     ^============================´         ^============================´         |    %s%s%s    |\n"), score == 4 ? fixCoding("^====================´") : "", score == 5 ? fixCoding("°====================`") : "", score != 4 && score != 5 ? "                      " : "");
  printf(fixCoding(" ====< ³ A: %-21s >=========< ³ B: %-21s >====     >==%s  4 ³       300 EUR %s==<\n"), fixCoding(answers[0]), fixCoding(answers[1]), score == 4 ? fixCoding("==<") : "   ", score == 4 ? fixCoding(">==") : "   ");
  printf(fixCoding("     °============================`         °============================`         |    %s%s%s    |\n"), score == 3 ? fixCoding("^====================´") : "", score == 4 ? fixCoding("°====================`") : "", score != 3 && score != 4 ? "                      " : "");
  printf(fixCoding("     ^============================´         ^============================´         >==%s  3 ³       200 EUR %s==<\n"), score == 3 ? fixCoding("==<") : "   ", score == 3 ? fixCoding(">==") : "   ");
  printf(fixCoding(" ====< ³ C: %-21s >=========< ³ D: %-21s >====     |    %s%s%s    |\n"), fixCoding(answers[2]), fixCoding(answers[3]), score == 2 ? fixCoding("^====================´") : "", score == 3 ? fixCoding("°====================`") : "", score != 2 && score != 3 ? "                      " : "");
  printf(fixCoding("     °============================`         °============================`         >==%s  2 ³       100 EUR %s==<\n"), score == 2 ? fixCoding("==<") : "   ", score == 2 ? fixCoding(">==") : "   ");
  printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 1 ? fixCoding("^====================´") : "", score == 2 ? fixCoding("°====================`") : "", score > 2 ? "                      " : "");
}

/**
 * Shows the answer view
 */
int showGameInterface(char *question, char *answers[4], bool bubble) {
  // Show question
  showQuestion(question, answers, bubble);

  bool joker = fiftyfifty || telefon || internet;
  printf(fixCoding("                         ^===´^===´^===´^===´ %s ^========´               >==%s  1 ³        50 EUR %s==<\n"), joker ? fixCoding("^=========´") : "           ", score == 1 ? fixCoding("==<") : "   ", score == 1 ? fixCoding(">==") : "   ");
  printf(fixCoding(" Mögliche Eingaben sind: | a || b || c || d | %s | (e)nde |               |    %s    |\n"), joker ? fixCoding("| (j)oker |") : "           ", score == 1 ? fixCoding("°====================`") : "                      ");
  printf(fixCoding("                         °===`°===`°===`°===` %s °========`               °==============================`\n"), joker ? fixCoding("°=========`") : "           ");
  printf(fixCoding(" Ihre Eingabe: "));

  // Get answer
  char input = getchar();

  // To consume `\n`
  if(input != '\n') getchar();

  switch(input) {
  case 'a': return 0;
  case 'b': return 1;
  case 'c': return 2;
  case 'd': return 3;

  case 'e':
    return -1;
    break;

  case 'j':
    if(joker) return showJokerInterface(question, answers);

  default:
    return showGameInterface(question, answers);
    break;
  }
}

/**
 * Shows the joker view
 */
int showJokerInterface(char *question, char *answers[4]) {
  showQuestion(question, answers);
  printf(fixCoding("                         %s%s%s                                           >==%s  1 ³        50 EUR %s==<\n"), fiftyfifty ? fixCoding("^===´") : fixCoding("^==/´"), telefon ? fixCoding("^===´") : fixCoding("^==/´"), internet ? fixCoding("^===´") : fixCoding("^==/´"), score == 1 ? fixCoding("==<") : "   ", score == 1 ? fixCoding(">==") : "   ");
  printf(fixCoding(" Mögliche Joker sind:    %s%s%s                                           |    %s    |\n"), fiftyfifty ? fixCoding("| 5 |") : fixCoding("| / |"), telefon ? fixCoding("| t |") : fixCoding("| / |"), internet ? fixCoding("| i |") : fixCoding("| / |"), score == 1 ? fixCoding("°====================`") : "                      ");
  printf(fixCoding("                         %s%s%s                                           °==============================`\n"), fiftyfifty ? fixCoding("°===`") : fixCoding("°/==`"), telefon ? fixCoding("°===`") : fixCoding("°/==`"), internet ? fixCoding("°===`") : fixCoding("°/==`"));
  printf(fixCoding(" Ihre Eingabe: "));

  char input = getchar();

  // To consume `\n`
  if(input != '\n') getchar();

  switch(input) {
  case '5':
    if(fiftyfifty == 0) return showJokerInterface(question, answers);

    fiftyfifty = 0;

    {
      int del1 = random(4);
      if(del1 == currentRightAnswer)
        del1 += 1;

      int del2 = del1 + 1;
      if(del2 % 4 == currentRightAnswer)
        del2 += 1;

      answers[del1 % 4] = "";
      answers[del2 % 4] = "";
    }

    return showGameInterface(question, answers);

  case 't':
    if(telefon == 0) return showJokerInterface(question, answers);

    telefon = 0;

    return showGameInterface(question, answers, true);

  case 'i':
    if(internet == 0) return showJokerInterface(question, answers);

    internet = 0;

    showQuestion(question, answers);
    printf(fixCoding("                                                                                   >==%s  1 ³        50 EUR %s==<\n"), score == 1 ? fixCoding("==<") : "   ", score == 1 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s    |\n"), score == 1 ? fixCoding("°====================`") : "                      ");
    printf(fixCoding("                                                                                   °==============================`\n"));
    printf(fixCoding(" Sie haben eine Minute Zeit, das Internet zu benutzen! Die Zeit läuft..."));
    Sleep(5000);

    return showGameInterface(question, answers);

  default:
    return showGameInterface(question, answers);
  }
}

void showEndInterface(int score, bool happy) {
  system("cls");
  if(score == 0) {
    printf(fixCoding("     ^=====%c%s´^=====%c%s´^=====%c%s=´                                            ^==============================´\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("     |  50%c%s || Tel%c%s || Int%c%st |                                            |                              |\n"), fiftyfifty ? ':' : '/', "50 ", telefon ? 'e' : '/', "fon", internet ? 'e' : '/', "rne");
    printf(fixCoding("     °===%c%s==`°===%c%s==`°===%c%s===`                                            >==    15 ³ 1.000.000 EUR    ==<\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==    14 ³   500.000 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                ________                         __          __                    >==    13 ³   125.000 EUR    ==<\n"));
    printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |                              |\n"));
    printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==    12 ³    64.000 EUR    ==<\n"));
    printf(fixCoding("               |________||_____|__|   |________||__||__| |_____|                   |                              |\n"));
    printf(fixCoding("                                                                                   >==    11 ³    32.000 EUR    ==<\n"));
    printf(fixCoding("               _______ __ __ __              .==.==.       _____                   |                              |\n"));
    printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==    10 ")); if(!risiko) setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("    16.000 EUR    ==<\n"));
    printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |                              |\n"));
    printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==     9 ³     8.000 EUR    ==<\n"));
    printf(fixCoding("                                                           '=='                    |                              |\n"));
    printf(fixCoding("                                                                                   >==     8 ³     4.000 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("     ^===================================================================´         >==     7 ³     2.000 EUR    ==<\n"));
    printf(fixCoding("     |                                                                   |         |                              |\n"));
    printf(fixCoding("     |  Du hast leider nichts gewonnen. Probiere es doch noch ein mal!   |         >==     6 ³     1.000 EUR    ==<\n"));
    printf(fixCoding("     |                                                                   |         |                              |\n"));
    printf(fixCoding("     °===================================================================`         >==     5 ")); setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("       500 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     4 ³       300 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     3 ³       200 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     2 ³       100 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                         ^===========´^========´                                   >==     1 ³        50 EUR    ==<\n"));
    printf(fixCoding(" Mögliche Eingaben sind: | (r)estart || e(x)it |                                   |                              |\n"));
    printf(fixCoding("                         °===========`°========`                                   °==============================`\n"));
    printf(fixCoding(" Ihre Eingabe: "));
  }
  else if(score == 15) {
    printf(fixCoding("     ^=====%c%s´^=====%c%s´^=====%c%s=´                                            ^==============================´\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("     |  50%c%s || Tel%c%s || Int%c%st |                                            |    ^====================´    |\n"), fiftyfifty ? ':' : '/', "50 ", telefon ? 'e' : '/', "fon", internet ? 'e' : '/', "rne");
    printf(fixCoding("     °===%c%s==`°===%c%s==`°===%c%s===`                                            >====< 15 ³ 1.000.000 EUR >====<\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("                                                                                   |    °====================`    |\n"));
    printf(fixCoding("                                                                                   >==    14 ³   500.000 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                ________                         __          __                    >==    13 ³   125.000 EUR    ==<\n"));
    printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |                              |\n"));
    printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==    12 ³    64.000 EUR    ==<\n"));
    printf(fixCoding("               |________||_____|__|   |________||__||__| |_____|                   |                              |\n"));
    printf(fixCoding("                                                                                   >==    11 ³    32.000 EUR    ==<\n"));
    printf(fixCoding("               _______ __ __ __              .==.==.       _____                   |                              |\n"));
    printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==    10 ")); if(!risiko) setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("    16.000 EUR    ==<\n"));
    printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |                              |\n"));
    printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==     9 ³     8.000 EUR    ==<\n"));
    printf(fixCoding("                                                           '=='                    |                              |\n"));
    printf(fixCoding("                                                                                   >==     8 ³     4.000 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("     ^===================================================================´         >==     7 ³     2.000 EUR    ==<\n"));
    printf(fixCoding("     |                     Glückwunsch! Du hast die                      |         |                              |\n"));
    printf(fixCoding("     |                           1.000.000 EUR                           |         >==     6 ³     1.000 EUR    ==<\n"));
    printf(fixCoding("     |                             gewonnen!                             |         |                              |\n"));
    printf(fixCoding("     °===================================================================`         >==     5 ")); setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("       500 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     4 ³       300 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     3 ³       200 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                                                                                   >==     2 ³       100 EUR    ==<\n"));
    printf(fixCoding("                                                                                   |                              |\n"));
    printf(fixCoding("                         ^===========´^========´                                   >==     1 ³        50 EUR    ==<\n"));
    printf(fixCoding(" Mögliche Eingaben sind: | (r)estart || e(x)it |                                   |                              |\n"));
    printf(fixCoding("                         °===========`°========`                                   °==============================`\n"));
    printf(fixCoding(" Ihre Eingabe: "));
  }
  else if(happy) {
    char *winSum = NULL;
    if(score == 1) winSum = "   50 EUR    ";
    if(score == 2) winSum = "   100 EUR   ";
    if(score == 3) winSum = "   200 EUR   ";
    if(score == 4) winSum = "   300 EUR   ";
    if(score == 5) winSum = "   500 EUR   ";
    if(score == 6) winSum = "  1.000 EUR  ";
    if(score == 7) winSum = "  2.000 EUR  ";
    if(score == 8) winSum = "  4.000 EUR  ";
    if(score == 9) winSum = "  8.000 EUR  ";
    if(score == 10) winSum = "  16.000 EUR ";
    if(score == 11) winSum = "  32.000 EUR ";
    if(score == 12) winSum = "  64.000 EUR ";
    if(score == 13) winSum = " 125.000 EUR ";
    if(score == 14) winSum = " 500.000 EUR ";

    printf(fixCoding("     ^=====%c%s´^=====%c%s´^=====%c%s=´                                            ^==============================´\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("     |  50%c%s || Tel%c%s || Int%c%st |                                            |    %s    |\n"), fiftyfifty ? ':' : '/', "50 ", telefon ? 'e' : '/', "fon", internet ? 'e' : '/', "rne", score == 15 ? fixCoding("^====================´") : "                      ");
    printf(fixCoding("     °===%c%s==`°===%c%s==`°===%c%s===`                                            >==%s 15 ³ 1.000.000 EUR %s==<\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="), score == 15 ? fixCoding("==<") : "   ", score == 15 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 14 ? fixCoding("^====================´") : "", score == 15 ? fixCoding("°====================`") : "", score != 14 && score != 15 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s 14 ³   500.000 EUR %s==<\n"), score == 14 ? fixCoding("==<") : "   ", score == 14 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 13 ? fixCoding("^====================´") : "", score == 14 ? fixCoding("°====================`") : "", score != 13 && score != 14 ? "                      " : "");
    printf(fixCoding("                ________                         __          __                    >==%s 13 ³   125.000 EUR %s==<\n"), score == 13 ? fixCoding("==<") : "   ", score == 13 ? fixCoding(">==") : "   ");
    printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |    %s%s%s    |\n"), score == 12 ? fixCoding("^====================´") : "", score == 13 ? fixCoding("°====================`") : "", score != 12 && score != 13 ? "                      " : "");
    printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==%s 12 ³    64.000 EUR %s==<\n"), score == 12 ? fixCoding("==<") : "   ", score == 12 ? fixCoding(">==") : "   ");
    printf(fixCoding("               |________||_____|__|   |________||__||__| |_____|                   |    %s%s%s    |\n"), score == 11 ? fixCoding("^====================´") : "", score == 12 ? fixCoding("°====================`") : "", score != 11 && score != 12 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s 11 ³    32.000 EUR %s==<\n"), score == 11 ? fixCoding("==<") : "   ", score == 11 ? fixCoding(">==") : "   ");
    printf(fixCoding("               _______ __ __ __              .==.==.       _____                   |    %s%s%s    |\n"), score == 10 ? fixCoding("^====================´") : "", score == 11 ? fixCoding("°====================`") : "", score != 10 && score != 11 ? "                      " : "");
    printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==%s 10 "), score == 10 ? fixCoding("==<") : "   "); if(!risiko) setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("    16.000 EUR %s==<\n"), score == 10 ? fixCoding(">==") : "   ");
    printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |    %s%s%s    |\n"), score == 9 ? fixCoding("^====================´") : "", score == 10 ? fixCoding("°====================`") : "", score != 9 && score != 10 ? "                      " : "");
    printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==%s  9 ³     8.000 EUR %s==<\n"), score == 9 ? fixCoding("==<") : "   ", score == 9 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                           '=='                    |    %s%s%s    |\n"), score == 8 ? fixCoding("^====================´") : "", score == 9 ? fixCoding("°====================`") : "", score != 8 && score != 9 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  8 ³     4.000 EUR %s==<\n"), score == 8 ? fixCoding("==<") : "   ", score == 8 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 7 ? fixCoding("^====================´") : "", score == 8 ? fixCoding("°====================`") : "", score != 7 && score != 8 ? "                      " : "");
    printf(fixCoding("     ^===================================================================´         >==%s  7 ³     2.000 EUR %s==<\n"), score == 7 ? fixCoding("==<") : "   ", score == 7 ? fixCoding(">==") : "   ");
    printf(fixCoding("     |                       Glückwunsch! Du hast                        |         |    %s%s%s    |\n"), score == 6 ? fixCoding("^====================´") : "", score == 7 ? fixCoding("°====================`") : "", score != 6 && score != 7 ? "                      " : "");
    printf(fixCoding("     |                           %s                           |         >==%s  6 ³     1.000 EUR %s==<\n"), winSum, score == 6 ? fixCoding("==<") : "   ", score == 6 ? fixCoding(">==") : "   ");
    printf(fixCoding("     |                             gewonnen!                             |         |    %s%s%s    |\n"), score == 5 ? fixCoding("^====================´") : "", score == 6 ? fixCoding("°====================`") : "", score != 5 && score != 6 ? "                      " : "");
    printf(fixCoding("     °===================================================================`         >==%s  5 "), score == 5 ? fixCoding("==<") : "   "); setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("       500 EUR %s==<\n"), score == 5 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 4 ? fixCoding("^====================´") : "", score == 5 ? fixCoding("°====================`") : "", score != 4 && score != 5 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  4 ³       300 EUR %s==<\n"), score == 4 ? fixCoding("==<") : "   ", score == 4 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 3 ? fixCoding("^====================´") : "", score == 4 ? fixCoding("°====================`") : "", score != 3 && score != 4 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  3 ³       200 EUR %s==<\n"), score == 3 ? fixCoding("==<") : "   ", score == 3 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 2 ? fixCoding("^====================´") : "", score == 3 ? fixCoding("°====================`") : "", score != 2 && score != 3 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  2 ³       100 EUR %s==<\n"), score == 2 ? fixCoding("==<") : "   ", score == 2 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 1 ? fixCoding("^====================´") : "", score == 2 ? fixCoding("°====================`") : "", score > 2 ? "                      " : "");
    printf(fixCoding("                         ^===========´^========´                                   >==%s  1 ³        50 EUR %s==<\n"), score == 1 ? fixCoding("==<") : "   ", score == 1 ? fixCoding(">==") : "   ");
    printf(fixCoding(" Mögliche Eingaben sind: | (r)estart || e(x)it |                                   |    %s    |\n"), score == 1 ? fixCoding("°====================`") : "                      ");
    printf(fixCoding("                         °===========`°========`                                   °==============================`\n"));
    printf(fixCoding(" Ihre Eingabe: "));
  }
  else {
    char *winSum = NULL;

    if(score < 5) winSum = "    nichts   ";
    if(score < 10) winSum = "   500 EUR   ";
    if(score >= 10 && risiko == 0) winSum = "  16.000 EUR ";

    score += 1;

    printf(fixCoding("     ^=====%c%s´^=====%c%s´^=====%c%s=´                                            ^==============================´\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="));
    printf(fixCoding("     |  50%c%s || Tel%c%s || Int%c%st |                                            |    %s    |\n"), fiftyfifty ? ':' : '/', "50 ", telefon ? 'e' : '/', "fon", internet ? 'e' : '/', "rne", score == 15 ? fixCoding("^====================´") : "                      ");
    printf(fixCoding("     °===%c%s==`°===%c%s==`°===%c%s===`                                            >==%s 15 ³ 1.000.000 EUR %s==<\n"), fiftyfifty ? HL : '/', fixCoding("==="), telefon ? HL : '/', fixCoding("==="), internet ? HL : '/', fixCoding("==="), score == 15 ? fixCoding("==<") : "   ", score == 15 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 14 ? fixCoding("^====================´") : "", score == 15 ? fixCoding("°====================`") : "", score != 14 && score != 15 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s 14 ³   500.000 EUR %s==<\n"), score == 14 ? fixCoding("==<") : "   ", score == 14 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 13 ? fixCoding("^====================´") : "", score == 14 ? fixCoding("°====================`") : "", score != 13 && score != 14 ? "                      " : "");
    printf(fixCoding("                ________                         __          __                    >==%s 13 ³   125.000 EUR %s==<\n"), score == 13 ? fixCoding("==<") : "   ", score == 13 ? fixCoding(">==") : "   ");
    printf(fixCoding("               |  |  |  |.=====.====. .==.==.==.|__|.====.==|  |                   |    %s%s%s    |\n"), score == 12 ? fixCoding("^====================´") : "", score == 13 ? fixCoding("°====================`") : "", score != 12 && score != 13 ? "                      " : "");
    printf(fixCoding("               |  |  |  ||  =__|   _| |  |  |  ||  ||   _|  _  |                   >==%s 12 ³    64.000 EUR %s==<\n"), score == 12 ? fixCoding("==<") : "   ", score == 12 ? fixCoding(">==") : "   ");
    printf(fixCoding("               |________||_____|__|   |________||__||__| |_____|                   |    %s%s%s    |\n"), score == 11 ? fixCoding("^====================´") : "", score == 12 ? fixCoding("°====================`") : "", score != 11 && score != 12 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s 11 ³    32.000 EUR %s==<\n"), score == 11 ? fixCoding("==<") : "   ", score == 11 ? fixCoding(">==") : "   ");
    printf(fixCoding("               _______ __ __ __              .==.==.       _____                   |    %s%s%s    |\n"), score == 10 ? fixCoding("^====================´") : "", score == 11 ? fixCoding("°====================`") : "", score != 10 && score != 11 ? "                      " : "");
    printf(fixCoding("              |   |   |__|  |  |.=====.=====.|===.=|.====.|__   |                  >==%s 10 "), score == 10 ? fixCoding("==<") : "   "); if(!risiko) setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("    16.000 EUR %s==<\n"), score == 10 ? fixCoding(">==") : "   ");
    printf(fixCoding("              |       |  |  |  ||  _  |     ||  _  ||   _|',  ,='                  |    %s%s%s    |\n"), score == 9 ? fixCoding("^====================´") : "", score == 10 ? fixCoding("°====================`") : "", score != 9 && score != 10 ? "                      " : "");
    printf(fixCoding("              |__|_|__|__|__|__||_____|__|__||___._||__|   |==|                    >==%s  9 ³     8.000 EUR %s==<\n"), score == 9 ? fixCoding("==<") : "   ", score == 9 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                           '=='                    |    %s%s%s    |\n"), score == 8 ? fixCoding("^====================´") : "", score == 9 ? fixCoding("°====================`") : "", score != 8 && score != 9 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  8 ³     4.000 EUR %s==<\n"), score == 8 ? fixCoding("==<") : "   ", score == 8 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 7 ? fixCoding("^====================´") : "", score == 8 ? fixCoding("°====================`") : "", score != 7 && score != 8 ? "                      " : "");
    printf(fixCoding("     ^===================================================================´         >==%s  7 ³     2.000 EUR %s==<\n"), score == 7 ? fixCoding("==<") : "   ", score == 7 ? fixCoding(">==") : "   ");
    printf(fixCoding("     |                      Falsche Antwort! Du hast                     |         |    %s%s%s    |\n"), score == 6 ? fixCoding("^====================´") : "", score == 7 ? fixCoding("°====================`") : "", score != 6 && score != 7 ? "                      " : "");
    printf(fixCoding("     |                           %s                           |         >==%s  6 ³     1.000 EUR %s==<\n"), winSum, score == 6 ? fixCoding("==<") : "   ", score == 6 ? fixCoding(">==") : "   ");
    printf(fixCoding("     |                              gewonnen!                            |         |    %s%s%s    |\n"), score == 5 ? fixCoding("^====================´") : "", score == 6 ? fixCoding("°====================`") : "", score != 5 && score != 6 ? "                      " : "");
    printf(fixCoding("     °===================================================================`         >==%s  5 "), score == 5 ? fixCoding("==<") : "   "); setColor(ORANGE); printf("%c", BULL); resetColor(); printf(fixCoding("       500 EUR %s==<\n"), score == 5 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 4 ? fixCoding("^====================´") : "", score == 5 ? fixCoding("°====================`") : "", score != 4 && score != 5 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  4 ³       300 EUR %s==<\n"), score == 4 ? fixCoding("==<") : "   ", score == 4 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 3 ? fixCoding("^====================´") : "", score == 4 ? fixCoding("°====================`") : "", score != 3 && score != 4 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  3 ³       200 EUR %s==<\n"), score == 3 ? fixCoding("==<") : "   ", score == 3 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 2 ? fixCoding("^====================´") : "", score == 3 ? fixCoding("°====================`") : "", score != 2 && score != 3 ? "                      " : "");
    printf(fixCoding("                                                                                   >==%s  2 ³       100 EUR %s==<\n"), score == 2 ? fixCoding("==<") : "   ", score == 2 ? fixCoding(">==") : "   ");
    printf(fixCoding("                                                                                   |    %s%s%s    |\n"), score == 1 ? fixCoding("^====================´") : "", score == 2 ? fixCoding("°====================`") : "", score > 2 ? "                      " : "");
    printf(fixCoding("                         ^===========´^========´                                   >==%s  1 ³        50 EUR %s==<\n"), score == 1 ? fixCoding("==<") : "   ", score == 1 ? fixCoding(">==") : "   ");
    printf(fixCoding(" Mögliche Eingaben sind: | (r)estart || e(x)it |                                   |    %s    |\n"), score == 1 ? fixCoding("°====================`") : "                      ");
    printf(fixCoding("                         °===========`°========`                                   °==============================`\n"));
    printf(fixCoding(" Ihre Eingabe: "));
  }
  char input = getchar();

  // To consume `\n`
  if(input != '\n') getchar();

  if(input != 'r') return;

  // Reset game
  fiftyfifty = 1;
  telefon = 1;
  startGame();
}



/*********************************
 * Question control functions    *
 *********************************/

 /**
  * This function creates a sample database
  */
void createDatabase(char *path) {
  showLoadingInterface();
  printf(" Erstelle Datenbank...");
  Sleep(1000);

  FILE *database;
  errno_t err = fopen_s(&database, path, "w");

  if(err != 0) {
    showLoadingInterface();
    printf(" Datenbank kann nicht erstellt werden, gib einen Pfad ein: ");

    char input[100];
    gets_s(input, 100);

    return createDatabase(input);
  }

  // Write sample data
  fprintf(database, "# Die Fragen sind wie folgt aufgebaut:");
  fprintf(database, "# Schwirigkeitsgrad(0-2); Frage; Richtige Antwort; Falsche Antwort; Falsche Antwort; Falsche Antwort;");
  fprintf(database, "# Achten Sie darauf, dass keine Leerzeichen um das Trennzeichen vorkommen und dass die Zeile mit einem Trennzeichen endet!");
  fprintf(database, "0;Preisgünstig, wenn auch mit weniger Komfort, reist man in der so genannten...;... Holzklasse;... Plastikklasse;... Steinklasse;... Pappklasse;");
  fprintf(database, "1;Wie heißt der König der Erdmännchen in der Augsburger Puppenkiste?;Kalle Wirsch;Klein Zack;Zwerg Nase;Frodo Beutlin;");
  fprintf(database, "2;Durch welches Verfahren schickte man im alten Athen seine Mitbürger in die Verbannung?;Scherbengericht;Götterspeise;Henkersmahlzeit;Grillteller;");

  fclose(database);

  showLoadingInterface();
  printf(" Leere Datenbank wurde erstellt! Datenbank: %s\n", path);
  printf(" Öffnen Sie die Datenbank und fügen Sie weitere Fragen hinzu!\n ");
  system("pause");
}

/**
 * This function loads the database and save the questions sortet by its level
 */
void loadDatabase(char *path) {
  showLoadingInterface();
  printf(" Lade Datenbank...");
  Sleep(1500);

  FILE *database;
  errno_t err = fopen_s(&database, path, "r");

  if(err != 0) {
    showLoadingInterface();
    printf(" Datenbank nicht gefunden, gib einen Pfad ein (oder tippe c|create): ");

    char input[100];
    gets_s(input, 100);

    if(input[0] == 'c')
      return createDatabase(path);

    return loadDatabase(input);
  }

  char buffer[255];
  int size_easy = 10;
  int size_middle = 10;
  int size_hard = 10;

  while(fgets(buffer, 255, database)) {
    if(buffer[0] == '#') continue;

    if(buffer[0] == '0') {
      if(i_easy + 1 >= size_easy) {
        size_easy += 10;
        data_easy = (char***)realloc(data_easy, size_easy*sizeof(char**));
      }

      char **line = str_split(buffer, ';', 6);
      data_easy[i_easy++] = line;
    }

    else if(buffer[0] == '1') {
      if(i_middle + 1 >= size_middle) {
        size_middle += 10;
        data_middle = (char***)realloc(data_middle, size_middle*sizeof(char**));
      }

      char **line = str_split(buffer, ';', 6);
      data_middle[i_middle++] = line;
    }

    else if(buffer[0] == '2') {
      if(i_hard + 1 >= size_hard) {
        size_hard += 10;
        data_hard = (char***)realloc(data_hard, size_hard*sizeof(char**));
      }

      char **line = str_split(buffer, ';', 6);
      data_hard[i_hard++] = line;
    }
  }

  fclose(database);
  startGame();
}

/**
 * Get a random Question from the data by level
 */
char** getRandomQuestion(int level, int ctr) {
  char **question = NULL;
  if(level == EASY) {
    int i = random(i_easy);
    question = data_easy[i];

    if(question == NULL && ctr < i_easy)
      return getRandomQuestion(level, ctr + 1);

    data_easy[i] = NULL;
  }
  else if(level == MIDDLE) {
    int i = random(i_middle);
    question = data_middle[i];

    if(question == NULL && ctr < i_middle)
      return getRandomQuestion(level, ctr + 1);

    data_middle[i] = NULL;
  }
  else if(level == HARD) {
    int i = random(i_hard);
    question = data_hard[i];

    if(question == NULL && ctr < i_hard)
      return getRandomQuestion(level, ctr + 1);

    data_hard[i] = NULL;
  }

  if(question == NULL) {
    showLoadingInterface();
    printf(" Sie haben alle Fragen einmal beantwortet, daher gibt es leider keine neuen Fragen.\n Das Spiel endet hier...");
    system("pause > nul");
  }

  return question;
}



/*********************************
 * MAIN                          *
 *********************************/

unsigned __stdcall playSound(void* pArguments) {
  PlaySound(TEXT("sound1.wav"), NULL, SND_SYNC);
  _endthreadex(0);
  return 0;
}

int main() {
  srand(time(NULL));
  prepareConsole();

  showLoadingInterface();
  
  unsigned threadID;
  _beginthreadex(NULL, 0, &playSound, NULL, 0, &threadID);

  Sleep(1000);
  loadDatabase("../../database.txt");

  return 0;
}
